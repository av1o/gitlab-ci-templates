.auto-deploy:
  image: "${AUTO_DEVOPS_REGISTRY_GITLAB}registry.gitlab.com/av1o/auto-deploy-image:v3-1.25"
  dependencies: []
  variables:
    ROLLOUT_RESOURCE_TYPE: 'deployment'

.default_rules:
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$DEPLOY_DISABLED || $CI_DEPLOY_FREEZE'
      when: never

review:
  extends: .auto-deploy
  stage: review
  script:
    - |
      if [ -n "$KUBE_CONTEXT" ]; then
        echo "Setting kubernetes context to $KUBE_CONTEXT"
        kubectl config use-context "${KUBE_CONTEXT}"
      fi
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy create_secret
    - auto-deploy deploy
    - auto-deploy persist_environment_url
    - echo "A11Y_URL=https://$CI_PROJECT_ID-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN" >> review.env
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_PROJECT_ID-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN
    on_stop: stop_review
    auto_stop_in: 1 day
  artifacts:
    paths:
      - environment_url.txt
    reports:
      dotenv:
        - review.env
    when: always
  rules:
    - if: '$CI_PIPELINE_SOURCE != "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: never
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$DEPLOY_DISABLED || $CI_DEPLOY_FREEZE || $REVIEW_DISABLED'
      when: never
    - when: on_success

stop_review:
  extends: .auto-deploy
  stage: cleanup
  variables:
    GIT_STRATEGY: none
  script:
    - |
      if [ -n "$KUBE_CONTEXT" ]; then
        echo "Setting kubernetes context to $KUBE_CONTEXT"
        kubectl config use-context "${KUBE_CONTEXT}"
      fi
    - auto-deploy delete
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  dependencies: []
  needs:
    - review
  allow_failure: true
  rules:
    - if: '$CI_PIPELINE_SOURCE != "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: never
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$DEPLOY_DISABLED || $CI_DEPLOY_FREEZE || $REVIEW_DISABLED'
      when: never
    - when: manual

# Staging deploys are disabled by default since
# continuous deployment to production is enabled by default
# If you prefer to automatically deploy to staging and
# only manually promote to production, enable this job by setting
# STAGING_ENABLED.

staging:
  extends: .auto-deploy
  stage: staging
  resource_group: staging
  script:
    - |
      if [ -n "$KUBE_CONTEXT" ]; then
        echo "Setting kubernetes context to $KUBE_CONTEXT"
        kubectl config use-context "${KUBE_CONTEXT}"
      fi
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy create_secret
    - auto-deploy deploy
  environment:
    name: staging
    url: https://$CI_PROJECT_PATH_SLUG-staging.$KUBE_INGRESS_BASE_DOMAIN
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$DEPLOY_DISABLED || $CI_DEPLOY_FREEZE'
      when: never
    - if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
      when: never
    - if: '$STAGING_ENABLED'

# Canaries are disabled by default, but if you want them,
# and know what the downsides are, you can enable this by setting
# CANARY_ENABLED.

canary:
  extends: .auto-deploy
  stage: canary
  allow_failure: true
  resource_group: production
  script:
    - |
      if [ -n "$KUBE_CONTEXT" ]; then
        echo "Setting kubernetes context to $KUBE_CONTEXT"
        kubectl config use-context "${KUBE_CONTEXT}"
      fi
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy create_secret
    - auto-deploy deploy canary
  environment:
    name: production
    url: https://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$DEPLOY_DISABLED || $CI_DEPLOY_FREEZE'
      when: never
    - if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
      when: never
    - if: '$CANARY_ENABLED'
      when: manual

.production: &production_template
  extends: .auto-deploy
  stage: production
  script:
    - |
      if [ -n "$KUBE_CONTEXT" ]; then
        echo "Setting kubernetes context to $KUBE_CONTEXT"
        kubectl config use-context "${KUBE_CONTEXT}"
      fi
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy create_secret
    - auto-deploy deploy
    - auto-deploy delete canary
    - auto-deploy delete rollout
    - auto-deploy persist_environment_url
  environment:
    name: production
    url: https://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  artifacts:
    paths:
      - environment_url.txt
    when: always

production:
  <<: *production_template
  resource_group: production
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$DEPLOY_DISABLED || $CI_DEPLOY_FREEZE'
      when: never
    - if: '$STAGING_ENABLED'
      when: never
    - if: '$CANARY_ENABLED'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_ENABLED'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

production_manual:
  <<: *production_template
  resource_group: production
  allow_failure: false
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$DEPLOY_DISABLED || $CI_DEPLOY_FREEZE'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_ENABLED'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $STAGING_ENABLED'
      when: manual
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CANARY_ENABLED'
      when: manual

# This job implements incremental rollout on for every push to `master`.

.rollout: &rollout_template
  extends: .auto-deploy
  script:
    - |
      if [ -n "$KUBE_CONTEXT" ]; then
        echo "Setting kubernetes context to $KUBE_CONTEXT"
        kubectl config use-context "${KUBE_CONTEXT}"
      fi
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy create_secret
    - auto-deploy deploy rollout $ROLLOUT_PERCENTAGE
    - auto-deploy scale stable $((100-ROLLOUT_PERCENTAGE))
    - auto-deploy delete canary
    - auto-deploy persist_environment_url
  environment:
    name: production
    url: https://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  artifacts:
    paths:
      - environment_url.txt
    when: always

.manual_rollout_template: &manual_rollout_template
  <<: *rollout_template
  stage: production
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$DEPLOY_DISABLED || $CI_DEPLOY_FREEZE'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE == "timed"'
      when: never
    - if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
      when: never
    # $INCREMENTAL_ROLLOUT_ENABLED is for compatibility with pre-GitLab 11.4 syntax
    - if: '$INCREMENTAL_ROLLOUT_MODE == "manual" || $INCREMENTAL_ROLLOUT_ENABLED'
      when: manual

.timed_rollout_template: &timed_rollout_template
  <<: *rollout_template
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$DEPLOY_DISABLED || $CI_DEPLOY_FREEZE'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE == "manual"'
      when: never
    - if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE == "timed"'
      when: delayed
      start_in: 5 minutes

timed rollout 10%:
  <<: *timed_rollout_template
  stage: incremental rollout 10%
  variables:
    ROLLOUT_PERCENTAGE: 10

timed rollout 25%:
  <<: *timed_rollout_template
  stage: incremental rollout 25%
  variables:
    ROLLOUT_PERCENTAGE: 25

timed rollout 50%:
  <<: *timed_rollout_template
  stage: incremental rollout 50%
  variables:
    ROLLOUT_PERCENTAGE: 50

timed rollout 100%:
  <<: *timed_rollout_template
  <<: *production_template
  stage: incremental rollout 100%
  variables:
    ROLLOUT_PERCENTAGE: 100

rollout 10%:
  <<: *manual_rollout_template
  variables:
    ROLLOUT_PERCENTAGE: 10

rollout 25%:
  <<: *manual_rollout_template
  variables:
    ROLLOUT_PERCENTAGE: 25

rollout 50%:
  <<: *manual_rollout_template
  variables:
    ROLLOUT_PERCENTAGE: 50

rollout 100%:
  <<: *manual_rollout_template
  <<: *production_template
  allow_failure: false
