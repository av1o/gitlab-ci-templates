# AutoDeploy

This is a modified version of the base GitLab AutoDeploy however it has been configured for Helm3 deployments.
This means that Tiller is no longer involved in the deployment process.

See the default AutoDeploy variables.

## STAGINGv2

> STAGINGv2 is experimental

Staging v2 allows you to use the `staging` branch as a separate environment.
Deployments to `production` are automatically triggered by commits to `master` as the intention is that it will work due to being thoroughly tested in `staging`.

```
Master  Staging
|       |
|       . <- deployment to staging
.<------| <- deployment to production (via staging->master merge request)
|
```