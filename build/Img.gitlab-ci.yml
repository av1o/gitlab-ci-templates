.img:
  interruptible: true
  image:
    name: "${AUTO_DEVOPS_REGISTRY_GITLAB}registry.gitlab.com/av1o/base-images/cosign-img:latest"
    entrypoint: [""]
  variables:
    KUBERNETES_POD_ANNOTATIONS_1: "container.apparmor.security.beta.kubernetes.io/build=unconfined"
    KUBERNETES_POD_ANNOTATIONS_2: "container.seccomp.security.alpha.kubernetes.io/build=unconfined"
    IMG_CACHE: "true"
  before_script:
    - img version
    - |
      cat <<EOF
      This pipeline uses Img to build OCI images.
      Img is a wrapper for BuildKit (buildx) which is still considered experimental so YMMV.

      For more information check out https://github.com/genuinetools/img
      EOF
  script:
    - |
      if [[ -n "${HARBOR_HOST:-}" ]]; then
        echo "Detected Harbor integration - images will be pushed to Harbor instead of GitLab..."
        CI_REGISTRY_IMAGE="$HARBOR_HOST/$HARBOR_PROJECT/$CI_PROJECT_NAME"
      fi
      if [[ -n "${PROJECT_PATH// }" ]]; then
        CI_REGISTRY_IMAGE="$CI_REGISTRY_IMAGE/$PROJECT_PATH"
      fi
    - |
      if [ -n "${HARBOR_HOST:-}" ]; then
        echo "Injecting registry credentials for Harbor (${HARBOR_HOST})"
        echo "$HARBOR_PASSWORD" | img login --username="$HARBOR_USERNAME" --password-stdin "${HARBOR_HOST}"
      fi
      if [ -n "${IMG_REGISTRY:-}" ]; then
          echo "Injecting registry credentials for ${IMG_REGISTRY}"
          echo "$IMG_REGISTRY_PASSWORD" | img login --username="$IMG_REGISTRY_USER" --password-stdin "${IMG_REGISTRY}"
      fi
      echo "Injecting registry credentials for ${CI_REGISTRY}"
      echo "$CI_REGISTRY_PASSWORD" | img login --username="${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"
    - |
      if [[ "${IMG_CACHE:-}" == "true" ]]; then
          echo "Enabling registry-based caching..."
          EXTRA_ARGS="${EXTRA_ARGS} --cache-from=type=registry,ref=$CI_REGISTRY_IMAGE/cache:latest --cache-to=type=registry,mode=max,ref=$CI_REGISTRY_IMAGE/cache:latest"
      fi
    - IMG_BUILD_ARGS=$(env | sed -n "s/^IMG_ARG_\(.*\)$/\1/p")
    - |
      if [ -n "${IMG_BUILD_ARGS:-}" ]; then
          BUILD_ARGS=$(echo "${IMG_BUILD_ARGS}" | tr ',' '\n' | while read build_arg; do echo "--build-arg=${build_arg}"; done)
      fi
    - CONTEXT=${IMG_CONTEXT:-$CI_PROJECT_DIR/$PROJECT_PATH}
    - DOCKERFILE=$CONTEXT/${IMG_DOCKERFILE:-Dockerfile}
    - DESTINATIONS="-t $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA -t $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - TAGS="$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - |
      if [ -n "${IMG_TAGS:-}" ]; then
          DESTINATIONS="${DESTINATIONS} $(echo "${IMG_TAGS}" | tr ',' '\n' | while read tag; do echo "-t $CI_REGISTRY_IMAGE:$tag "; done)"
          TAGS="${TAGS} $(echo "${IMG_TAGS}" | tr ',' '\n' | while read tag; do echo " $CI_REGISTRY_IMAGE:$tag"; done)"
      fi
    - DESTINATIONS="${DESTINATIONS} -t $CI_REGISTRY_IMAGE:latest"
    - TAGS="${TAGS} $CI_REGISTRY_IMAGE:latest"
    - |
      if [ -n "${CI_COMMIT_BRANCH:-}" ]; then
          DESTINATIONS="${DESTINATIONS} -t $CI_REGISTRY_IMAGE:${CI_COMMIT_BRANCH/\//-}"
          TAGS="${TAGS} $CI_REGISTRY_IMAGE:${CI_COMMIT_BRANCH/\//-}"
      fi
    - |
      if [ -n "${CI_COMMIT_TAG:-}" ]; then
          DESTINATIONS="${DESTINATIONS} -t $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG"
          TAGS="${TAGS} $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG"
      fi
    - echo "Building tags ${TAGS}"
    - img build ${EXTRA_ARGS:-} ${BUILD_ARGS:-} ${DESTINATIONS} -f "$DOCKERFILE" "$CONTEXT"
    - echo "${TAGS}" | tr -s ' ' '\n' | while read tag; do img push "${tag}"; done
    - |
      if ! command -v cosign &> /dev/null; then
        echo "Skipping signing as cosign is not available"
        exit 0
      fi
      if [[ -z "$CNB_SIGN_ENABLED" || -z "$COSIGN_PRIVATE_KEY" || -z "$COSIGN_PASSWORD" ]]; then
        echo "Skipping signing (make sure CNB_SIGN_ENABLED, COSIGN_PRIVATE_KEY, COSIGN_PASSWORD are set)"
        exit 0
      fi
      echo "Using cosign key: $COSIGN_PRIVATE_KEY"
      for image in ${TAGS}; do
        echo "Signing image: '$image'"
        cosign sign \
          --key "$COSIGN_PRIVATE_KEY" \
          -a "ci.commit.sha=$CI_COMMIT_SHA" \
          -a "ci.commit.ref_slug=$CI_COMMIT_REF_SLUG" \
          -a "ci.job.finished_at=$(date)" \
          -a "ci.job.url=$CI_JOB_URL" \
          -a "ci.job.image=$CI_JOB_IMAGE" \
          -a "ci.project.url=$CI_PROJECT_URL" \
          -a "ci.project.classification=${CI_PROJECT_CLASSIFICATION_LABEL:-"none"}" \
          -a "ci.pipeline.url=$CI_PIPELINE_URL" \
          -a "ci.runner.version=$CI_RUNNER_VERSION" \
          "$image"
      done
  rules:
    - if: '$CI_OPEN_MERGE_REQUESTS != null && $CI_PIPELINE_SOURCE == "push"'
      when: never
    - if: '$CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null && $CI_OPEN_MERGE_REQUESTS == null'
      when: never
    - if: '$BUILD_DISABLED != null'
      when: never
    - when: on_success
