.kaniko:
  interruptible: true
  image:
    name: "${AUTO_DEVOPS_REGISTRY_GITLAB}registry.gitlab.com/av1o/base-images/cosign-kaniko:v1.9.0"
    entrypoint: [""]
  variables:
    KANIKO_CACHE: 'true'
  before_script:
    - /kaniko/executor version
  script:
    - mkdir -p /kaniko/.docker
    - |
      if [[ -n "${HARBOR_HOST:-}" ]]; then
        echo "Detected Harbor integration - images will be pushed to Harbor instead of GitLab..."
        CI_REGISTRY_IMAGE="$HARBOR_HOST/$HARBOR_PROJECT/$CI_PROJECT_NAME"
      fi
      if [[ -n "${PROJECT_PATH// }" ]]; then
        CI_REGISTRY_IMAGE="$CI_REGISTRY_IMAGE/$PROJECT_PATH"
      fi
    - |
      if [ -n "${HARBOR_HOST:-}" ]; then
          echo "Injecting registry creds for Harbor (${HARBOR_HOST}) and the GitLab Container Registry"
          echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}, \"$HARBOR_HOST\": {\"username\": \"$HARBOR_USERNAME\", \"password\": \"$HARBOR_PASSWORD\"}}}" > /kaniko/.docker/config.json
      elif [ -n "${KANIKO_REGISTRY:-}" ]; then
          echo "Injecting registry creds for ${KANIKO_REGISTRY} and the GitLab Container Registry"
          echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}, \"$KANIKO_REGISTRY\": {\"username\": \"$KANIKO_REGISTRY_USER\", \"password\": \"$KANIKO_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
      else
          echo "Injecting registry creds for the GitLab Container Registry"
          echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
      fi
    - |
      if [[ "${KANIKO_USE_NEW_ARGS:-}" == "true" ]]; then
          KANIKO_BUILD_ARGS=$(env | sed -n "s/^KANIKO_ARG_\(.*\)$/\1/p")
      fi
      if [ -n "${KANIKO_BUILD_ARGS:-}" ]; then
          BUILD_ARGS=$(echo "${KANIKO_BUILD_ARGS}" | tr ',' '\n' | while read build_arg; do echo "--build-arg=${build_arg}"; done)
      fi
    - |
      if [[ "${KANIKO_CACHE:-}" == "true" ]]; then
          CACHE="--cache=true"
      fi
    - |
      if [[ "${KANIKO_USE_NEW_RUN:-}" == "true" ]]; then
          EXTRA_ARGS="${EXTRA_ARGS} --use-new-run=true"
      fi
    - |
      if [ -n "${KANIKO_REGISTRY_MIRROR:-}" ]; then
          EXTRA_ARGS="${EXTRA_ARGS} --registry-mirror=${KANIKO_REGISTRY_MIRROR}"
      fi
    - export CONTEXT=${KANIKO_CONTEXT:-$CI_PROJECT_DIR/$PROJECT_PATH}
    - export DOCKERFILE=$CONTEXT/${KANIKO_DOCKERFILE:-Dockerfile}
    - |
      if [ -n "${KANIKO_TAGS:-}" ]; then
          DESTINATIONS=$(echo "${KANIKO_TAGS}" | tr ',' '\n' | while read tag; do echo "--destination $CI_REGISTRY_IMAGE:$tag "; done)
          TAGS=$(echo "${KANIKO_TAGS}" | tr ',' '\n' | while read tag; do echo " $CI_REGISTRY_IMAGE:$tag "; done)
      fi
    - DESTINATIONS="${DESTINATIONS} --destination $CI_REGISTRY_IMAGE:latest --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - TAGS="${TAGS} $CI_REGISTRY_IMAGE:latest $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - |
      if [ -n "${CI_COMMIT_BRANCH:-}" ]; then
          DESTINATIONS="${DESTINATIONS} --destination $CI_REGISTRY_IMAGE:${CI_COMMIT_BRANCH/\//-}"
          TAGS="${TAGS} $CI_REGISTRY_IMAGE:${CI_COMMIT_BRANCH/\//-}"
      fi
    - |
      if [ -n "${CI_COMMIT_TAG:-}" ]; then
          DESTINATIONS="${DESTINATIONS} --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG"
          TAGS="${TAGS} $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG"
      fi
    - |
      if [[ "${KANIKO_DEBUG:-}" == "true" ]]; then
          echo -e "--- DEBUG DEBUG DEBUG ---\nYou should only use this for testing. Set KANIKO_DEBUG to 'false' to remove this warning!\n\nBUILD-ARGS: ${BUILD_ARGS:-}\n--- DEBUG DEBUG DEBUG ---\n"
      fi
    - /kaniko/executor --context $CONTEXT --dockerfile $DOCKERFILE ${DESTINATIONS} ${CACHE:-} ${BUILD_ARGS:-} ${EXTRA_ARGS:-}
    - |
      if ! command -v cosign &> /dev/null; then
        echo "Skipping signing as cosign is not available"
        exit 0
      fi
      if [[ -z "$CNB_SIGN_ENABLED" || -z "$COSIGN_PRIVATE_KEY" || -z "$COSIGN_PASSWORD" ]]; then
        echo "Skipping signing (make sure CNB_SIGN_ENABLED, COSIGN_PRIVATE_KEY, COSIGN_PASSWORD are set)"
        exit 0
      fi
      echo "Using cosign key: $COSIGN_PRIVATE_KEY"
      for image in ${TAGS}; do
        echo "Signing image: '$image'"
        cosign sign \
          --key "$COSIGN_PRIVATE_KEY" \
          -a "ci.commit.sha=$CI_COMMIT_SHA" \
          -a "ci.commit.ref_slug=$CI_COMMIT_REF_SLUG" \
          -a "ci.job.finished_at=$(date)" \
          -a "ci.job.url=$CI_JOB_URL" \
          -a "ci.job.image=$CI_JOB_IMAGE" \
          -a "ci.project.url=$CI_PROJECT_URL" \
          -a "ci.project.classification=${CI_PROJECT_CLASSIFICATION_LABEL:-"none"}" \
          -a "ci.pipeline.url=$CI_PIPELINE_URL" \
          -a "ci.runner.version=$CI_RUNNER_VERSION" \
          "$image"
      done
  rules:
    - if: '$CI_OPEN_MERGE_REQUESTS != null && $CI_PIPELINE_SOURCE == "push"'
      when: never
    - if: '$CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null && $CI_OPEN_MERGE_REQUESTS == null'
      when: never
    - if: '$BUILD_DISABLED != null || $BUILD_KANIKO_DISABLED != null'
      when: never
    - when: on_success
