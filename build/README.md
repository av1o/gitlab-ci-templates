# Builders

## AutoBuild / CNB

CloudNative BuildPacks. Opt-in for Java, GoLang Auto pipelines.

Known issue with early versions of `containerd` (<`1.4.3`) due to the large number of image layers.

Does not require `root` privileges.

## Img

Build OCI images using the `img` tool. Experimental.

**Note: Img requires the running seccomp profile to be `unconfined`**

Does not require `root` privileges.

## Kaniko

Build OCI images using the `kaniko` tool. Default in all Auto pipelines.

Known issue with versions > `1.3.0` that causes layers to be dropped.
This project has pinned Kaniko to `1.3.0` until issues are resolved.

Requires `root` privileges to run.

## Makisu

Build OCI images using the `makisu` tool. Experimental.

Requires `root` privileges to run.

## Chart

Build and push non-OCI Helm charts to ChartMuseum.