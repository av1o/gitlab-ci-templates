variables:
  AUTO_DEVOPS_REGISTRY: "docker.io/"
  AUTO_DEVOPS_REGISTRY_GITLAB: ""
  AUTO_DEVOPS_CHART: charts/auto-deploy-app
  AUTO_DEVOPS_CHART_REPOSITORY: https://av1o.gitlab.io/charts
  AUTO_DEVOPS_CHART_REPOSITORY_NAME: charts
  AUTO_DEVOPS_DOCKERFILE: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/auto/gomod/Dockerfile'
  AUTO_DEVOPS_DOCKERIGNORE: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/auto/gomod/.dockerignore'
  AUTO_RELEASE_DISABLED: 'true'

stages:
  - prepare
  - verify
  - test
  - build
  - predeploy
  - deploy
  - review
  - staging
  - canary
  - production
  - incremental rollout 10%
  - incremental rollout 25%
  - incremental rollout 50%
  - incremental rollout 100%
  - performance
  - cleanup

prepare_resources:
  stage: prepare
  image:
    name: "${AUTO_DEVOPS_REGISTRY}curlimages/curl:7.75.0"
    entrypoint: [""]
  dependencies: []
  needs: []
  before_script:
    - curl --version
  script:
    - |
      if [[ ! -f "Dockerfile" ]]; then
        echo "Failed to locate existing Dockerfile"
        curl -L ${AUTO_DEVOPS_DOCKERFILE} -o Dockerfile
      fi
    - |
      if [[ ! -f ".dockerignore" ]]; then
        echo "Failed to locate existing .dockerignore"
        curl -L ${AUTO_DEVOPS_DOCKERIGNORE} -o .dockerignore
      fi
    - |
      if [[ ! -f ".netrc" ]]; then
        echo "Failed to locate existing .netrc file, injecting contents of AUTO_DEVOPS_NETRC"
        echo "${AUTO_DEVOPS_NETRC}" > .netrc
      fi
  artifacts:
    paths:
      - Dockerfile
      - .dockerignore
      - .netrc
    expire_in: 1 day
  rules:
    - if: '$CI_OPEN_MERGE_REQUESTS != null && $CI_PIPELINE_SOURCE == "push"'
      when: never
    - if: '$CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null && $CI_OPEN_MERGE_REQUESTS == null'
      when: never
    - if: '$BUILD_KANIKO_DISABLED != null'
      when: never
    - when: on_success

autobuild golang:
  stage: build
  extends: .auto-build
  dependencies: []
  needs:
    - job: test_go
      optional: true
  rules:
    - if: '$CI_OPEN_MERGE_REQUESTS != null && $CI_PIPELINE_SOURCE == "push"'
      when: never
    - if: '$CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null && $CI_OPEN_MERGE_REQUESTS == null'
      when: never
    - if: '$BUILD_DISABLED != null || $BUILD_KANIKO_DISABLED == null'
      when: never
    - when: on_success

build golang:
  stage: build
  extends: .kaniko
  dependencies:
    - prepare_resources
  needs:
    - job: test_go
      optional: true
    - prepare_resources

include:
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/test/Go-Test.gitlab-ci.yml'
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/verify/bundles/GoLangCQ.gitlab-ci.yml'
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/build/Kaniko.gitlab-ci.yml'
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/build/CNB.gitlab-ci.yml'
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/deploy/Deploy.gitlab-ci.yml'
  - remote: "https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/verify/CodeIntel.gitlab-ci.yml"
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/workflows/Push-And-MR.gitlab-ci.yml'
  - remote: "https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/deploy/AutoRelease.gitlab-ci.yml"
  - remote: "https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/defend/Sast.gitlab-ci.yml"
  - remote: "https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/defend/ContainerScanning.gitlab-ci.yml"
