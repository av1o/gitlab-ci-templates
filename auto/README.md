# Auto Pipelines

* [AutoUI](AUTOUI.md) for deploying a static website
* [AutoMaven](AUTOJVM.md) for deploying a Maven app
* [AutoGradle](AUTOJVM.md) for deploying a Gradle app
* [AutoGoMod](AUTOGOMOD.md) for deploying a GoLang app using Go Modules
* [AutoPython](AUTOPYTHON.md) for deploying a Python app

### STAGINGv2

[What is STAGINGv2?](../deploy/README.md)

Due to its experimental nature, STAGINGv2 is not included by default.
In order to enable it, you will need to extend the default Auto pipeline.

**Example**

```yaml
include:
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/auto/Auto-Maven.gitlab-ci.yml'
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/deploy/Deploy2.gitlab-ci.yml' # override with the stagingv2 template
```