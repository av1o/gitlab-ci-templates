# Auto Python

This pipeline allows you to build a Python application and deploy it.

It automates the following stages:
1. Testing
1. Building a container image
1. Deploying to Kubernetes

Deployment uses the standard AutoDevOps deploy variables, however it has been configured to use Helm 3 instead of 2.

AutoPython uses CloudNative BuildPacks by default.

## Requirements

* `requirements.txt` at the top-level of the project.

## Example usage

Using Dockerfile:

```yaml
include:
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/auto/Auto-Python.gitlab-ci.yml'
```
