variables:
  AUTO_DEVOPS_REGISTRY: "docker.io/"
  AUTO_DEVOPS_REGISTRY_GITLAB: ""
  AUTO_DEVOPS_CHART: charts/auto-deploy-app
  AUTO_DEVOPS_CHART_REPOSITORY: https://av1o.gitlab.io/charts
  AUTO_DEVOPS_CHART_REPOSITORY_NAME: charts
  AUTO_UI_ENV_DOCKERFILE: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/auto/resources/env.Dockerfile'
  AUTO_UI_DOCKERFILE: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/auto/resources/Dockerfile'
  AUTO_UI_DOCKERIGNORE: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/auto/resources/.dockerignore'
  AUTO_UI_NGINX_CONF: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/auto/resources/nginx.conf'
  AUTO_UI_ENV_SCRIPT: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/auto/resources/env.sh'
  a11y_urls: "$ADDITIONAL_HOSTS $CI_ENVIRONMENT_URL"
  AUTO_RELEASE_DISABLED: 'true'

stages:
  - prepare
  - verify
  - test
  - build
  - deploy
  - predeploy
  - review
  - staging
  - canary
  - production
  - incremental rollout 10%
  - incremental rollout 25%
  - incremental rollout 50%
  - incremental rollout 100%
  - performance
  - accessibility
  - cleanup

prepare_resources:
  stage: prepare
  image:
    name: "${AUTO_DEVOPS_REGISTRY}curlimages/curl:7.75.0"
    entrypoint: [""]
  dependencies: []
  needs: []
  before_script:
    - curl --version
  script:
    - |
      if [[ ! -f "Dockerfile" ]]; then
        echo "Failed to locate existing Dockerfile"
        if [ -f .env ]; then
          echo "Detected .env file, downloading env-config Dockerfile..."
          curl -L ${AUTO_UI_ENV_DOCKERFILE} -o Dockerfile
          curl -L ${AUTO_UI_ENV_SCRIPT} -o env.sh
        else
          curl -L ${AUTO_UI_DOCKERFILE} -o Dockerfile
        fi
      fi
    - |
      if [[ ! -f ".dockerignore" ]]; then
        echo "Failed to locate existing .dockerignore"
        curl -L ${AUTO_UI_DOCKERIGNORE} -o .dockerignore
      fi
    - |
      if [[ ! -f "nginx.conf" ]]; then
        echo "Failed to locate existing nginx.conf"
        curl -L ${AUTO_UI_NGINX_CONF} -o nginx.conf
      fi
  artifacts:
    paths:
      - Dockerfile
      - .dockerignore
      - nginx.conf
      - env.sh
    expire_in: 1 day
  rules:
    - if: '$CI_OPEN_MERGE_REQUESTS != null && $CI_PIPELINE_SOURCE == "push"'
      when: never
    - if: '$CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null && $CI_OPEN_MERGE_REQUESTS == null'
      when: never
    - if: '$BUILD_KANIKO_DISABLED != null'
      when: never
    - when: on_success

autobuild node:
  stage: build
  extends: .auto-build
  dependencies: []
  variables:
    AUTO_CNB_BUILD_IMAGE: "${AUTO_DEVOPS_REGISTRY_GITLAB}registry.gitlab.com/autokubeops/buildpacks/lifecycle:0.3"
  needs:
    - job: test_node
      optional: true
  script:
    - ${AUTO_CNB_BUILD_ENV_VARS} /main
  rules:
    - if: '$CI_OPEN_MERGE_REQUESTS != null && $CI_PIPELINE_SOURCE == "push"'
      when: never
    - if: '$CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null && $CI_OPEN_MERGE_REQUESTS == null'
      when: never
    - if: '$BUILD_DISABLED != null || $BUILD_KANIKO_DISABLED == null'
      when: never
    - when: on_success

build node:
  stage: build
  extends: .kaniko
  dependencies:
    - prepare_resources
  needs:
    - job: test_node
      optional: true
    - prepare_resources

include:
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/test/Node-Test.gitlab-ci.yml'
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/verify/bundles/NodeCQ.gitlab-ci.yml'
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/build/Kaniko.gitlab-ci.yml'
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/build/CNB.gitlab-ci.yml'
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/deploy/Deploy.gitlab-ci.yml'
  - remote: "https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/verify/CodeIntel.gitlab-ci.yml"
  - remote: "https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/verify/Accessibility.gitlab-ci.yml"
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/workflows/Push-And-MR.gitlab-ci.yml'
  - remote: "https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/deploy/AutoRelease.gitlab-ci.yml"
  - remote: "https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/defend/ContainerScanning.gitlab-ci.yml"
