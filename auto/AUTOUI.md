# Auto UI

This pipeline allows you to build a UI application and deploy it.

It automates the following stages:
1. Testing
2. Building a container image
3. Deploying to Kubernetes

Deployment uses the standard AutoDevOps deploy variables, however it has been configured to use Helm 3 instead of 2.

## Requirements

* `package.json` at the top-level of the project.
* TypeScript.
* Static files must be output to the `./build` directory.

In order to generate an `env-config.js`:
* .env file at the top-level of the project.

## Configuration

`AUTO_UI_DOCKERFILE` - customise the URI which the `Dockerfile` is downloaded from.
`AUTO_UI_ENV_DOCKERFILE` - customise the URI which the `env-config` `Dockerfile` is downloaded from.
`AUTO_UI_DOCKERIGNORE` - customise the URI which the `.dockerignore` is downloaded from.

**Kaniko configuration**

AutoUI allows you to inject a number of [build-args](https://docs.docker.com/engine/reference/commandline/build/#set-build-time-variables---build-arg)
which can be used to override build behaviour.

`NPM_REGISTRY` - tell NPM to use a custom registry (default: `registry.npmjs.org`). Do not include the scheme (e.g. `https://`)

`NPM_TOKEN` - tell NPM to use an auth token when talking to the registry.

`NPM_EXTRA_ARGS` - extra arguments to pass to the `npm ci` command.

Using these values:

For examples:

```yaml
variables:
  KANIKO_USE_NEW_ARGS: "true"
  KANIKO_ARG_NPM_REGISTRY: "myregistry.com"
  KANIKO_ARG_NPM_EXTRA_ARGS: "--do-something-special"
```

## Example usage


```yaml
include:
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/auto/Auto-UI.gitlab-ci.yml'
```