# STAGE 1 - BUILD
ARG NODE_VERSION="lts"
FROM registry.gitlab.com/av1o/base-images/node:$NODE_VERSION

RUN mkdir -p /home/somebody/build
WORKDIR /home/somebody/build

# disable spammy donation messages
ENV DISABLE_OPENCOLLECTIVE=true
ENV GATSBY_TELEMETRY_DISABLED 1

# inject auth if it's present
ARG NPM_REGISTRY
ARG	NPM_TOKEN
ARG NPM_EXTRA_ARGS
RUN if [ -n "${NPM_TOKEN:-}" ]; then echo "Authenticating to registry: ${NPM_REGISTRY:-registry.npmjs.org}" && npm set "//${NPM_REGISTRY:-registry.npmjs.org}:_authToken" "${NPM_TOKEN}"; fi && \
    npm config set registry "https://${NPM_REGISTRY:-registry.npmjs.org}"

COPY package*.json ./
RUN npm ci ${NPM_EXTRA_ARGS}

COPY . .

RUN npm run build

# STAGE 2 - RUN
FROM registry.gitlab.com/av1o/base-images/nginx:stable-alpine

WORKDIR /app

# download the nginx conf
COPY --chown=somebody:0 ./nginx.conf .
# copy .env file and shell script
COPY --chown=somebody:0 ./env.sh .
COPY --chown=somebody:0 .env .

WORKDIR /var/www/html

COPY --chown=somebody:0 --from=0 /home/somebody/build/build/ .

RUN chmod 664 /app/nginx.conf && \
    chmod 774 /app/env.sh

VOLUME ["/var/cache/nginx", "/var/run", "/tmp"]
EXPOSE 8080

# start NGINX
CMD ["/bin/bash", "-c", "/app/env.sh && nginx -c /app/nginx.conf -g \"daemon off;\""]
