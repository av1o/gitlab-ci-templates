#!/bin/bash

# Recreate config file
rm -rf /var/www/html/env-config.js
touch /var/www/html/env-config.js

# Add assignment
echo "window._env_ = {" >> /var/www/html/env-config.js

# Read each line in .env file
# Each line represents key=value pairs
while read -r line || [[ -n "$line" ]];
do
  # Split env variables by character `=`
  if printf '%s\n' "$line" | grep -q -e '='; then
    varname=$(printf '%s\n' "$line" | sed -e 's/=.*//')
    varvalue=$(printf '%s\n' "$line" | sed -e 's/^[^=]*=//')
  fi

  # Read value of current variable if exists as Environment variable
  value=$(printf '%s\n' "${!varname}")
  # Otherwise use value from .env file
  [[ -z $value ]] && value=${varvalue}

  # Append configuration property to JS file
  echo "  $varname: \"$value\"," >> /var/www/html/env-config.js
done < /app/.env

echo "};" >> /var/www/html/env-config.js