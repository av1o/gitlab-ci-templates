# STAGE 1 - BUILD
ARG GRADLE_TAG="16-v7.0.2"
FROM registry.gitlab.com/av1o/base-images/gradle:${GRADLE_TAG}

# run gradle ins a user-owned directory
RUN mkdir -p /home/gradle/app
WORKDIR /home/gradle/app

ARG AUTO_DEVOPS_GRADLE_BUILD_ARGS
ARG GRADLE_OPTS
ARG CI_JOB_TOKEN

# Dry run for caching
COPY --chown=gradle:0 . .

# disable the gradle daemon
RUN GRADLE_OPTS=$(echo "${GRADLE_OPTS}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g') \
	gradle build -x test $(echo "${AUTO_DEVOPS_GRADLE_BUILD_ARGS}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g')

# STAGE 2 - RUN
ARG GRADLE_RUNTIME_TAG="16-v9"
FROM registry.gitlab.com/av1o/base-images/tomcat-native:${GRADLE_RUNTIME_TAG}

RUN mkdir -p /home/somebody/app
WORKDIR /home/somebody/app

ARG GRADLE_MODULE="."
COPY --from=0 --chown=somebody:0 /home/gradle/app/build/${GRADLE_MODULE}/libs/*.jar /home/somebody/app/app.jar

EXPOSE 8080

RUN chown -R somebody:0 /home/somebody/app && \
	chmod -R g=u /home/somebody/app
USER somebody

VOLUME ["/tmp"]

CMD ["java", "--enable-preview", "-jar", "/home/somebody/app/app.jar"]
