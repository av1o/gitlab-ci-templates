# STAGE 1 - BUILD
ARG MAVEN_TAG="17"
FROM registry.gitlab.com/av1o/base-images/maven:${MAVEN_TAG}

# run maven in a user-owned directory
RUN mkdir -p /home/maven/app
WORKDIR /home/maven/app

ARG MAVEN_SETTINGS_ARGS
ARG AUTO_DEVOPS_MAVEN_BUILD_ARGS
ARG MAVEN_OPTS
ARG CI_JOB_TOKEN

# setup the settings.xml and trim quotes if needed
RUN /opt/mvn-settings-gen $(echo "${MAVEN_SETTINGS_ARGS}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g') > ci_settings.xml

# Dry run for caching
COPY --chown=maven:0 . .

RUN MAVEN_OPTS=$(echo "${MAVEN_OPTS}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g') \
	mvn --settings ci_settings.xml --batch-mode package -Dmaven.test.skip=true $(echo "${AUTO_DEVOPS_MAVEN_BUILD_ARGS}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g')

# STAGE 2 - RUN
ARG MAVEN_RUNTIME_TAG="17"
FROM registry.gitlab.com/av1o/base-images/tomcat-native:${MAVEN_RUNTIME_TAG}

RUN mkdir -p /home/somebody/app
WORKDIR /home/somebody/app

ARG MAVEN_MODULE="."
COPY --from=0 --chown=somebody:0 /home/maven/app/${MAVEN_MODULE}/target/*.jar /home/somebody/app/app.jar

EXPOSE 8080

RUN chown -R somebody:0 /home/somebody/app && \
	chmod -R g=u /home/somebody/app
USER somebody

VOLUME ["/tmp"]

CMD ["java", "--enable-preview", "-jar", "/home/somebody/app/app.jar"]
