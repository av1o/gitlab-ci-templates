# Auto Go Mod

This pipeline allows you to build a Go (go modules) application and deploy it.

It automates the following stages:
1. Testing
1. Code quality scans
1. Building a container image
1. Deploying to Kubernetes

Deployment uses the standard AutoDevOps deploy variables, however it has been configured to use Helm 3 instead of 2.

## Requirements

* `go.mod` `go.sum` at the top-level of the project.

## Configuration

`AUTO_GOMOD_DOCKERFILE` - customise the URI which the `Dockerfile` is downloaded from.
`AUTO_GOMOD_DOCKERIGNORE` - customise the URI which the `.dockerignore` is downloaded from.

## Example usage

Using Dockerfile:

```yaml
include:
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/auto/Auto-GoMod.gitlab-ci.yml'
```

Using CloudNative BuildPacks:

```yaml
variables:
  AUTO_CNB_RUN_IMAGE: gcr.io/paketo-buildpacks/run:tiny-cnb
  BUILD_KANIKO_DISABLED: "true"

include:
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/auto/Auto-GoMod.gitlab-ci.yml'
```