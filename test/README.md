# AutoTest

GitLab CI templates for automatic testing.

Supports the following frameworks/languages:

* Gradle
* Maven
* NodeJS
* GoLang
* Python


*Note:* Node tests get used for a [create-react-app](https://github.com/facebook/create-react-app) Jest app. YMMV

## Usage

This is the minimal `.gitlab-ci.yml` required to include AutoTest.

```yaml
stages:
  - test

include:
  - remote: "https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/test/AutoTest.gitlab-ci.yml"
```

AutoTest will figure out how to run your tests based on the files available in your repo. 
For example if a pom.xml is present, Maven tests will be run.

### Configuration

`TEST_DISABLED` - if present, this stage will be skipped

`AUTO_DEVOPS_GRADLE_EXTRA_ARGS` - additional arguments passed to Gradle

`AUTO_DEVOPS_MAVEN_EXTRA_ARGS` - additional arguments passed to Maven

`AUTO_DEVOPS_NODE_EXTRA_ARGS` - additional arguments passed to NPM

`AUTO_DEVOPS_NODE_REGISTRY` - custom NPM registry (default: `registry.npmjs.org`)

`AUTO_DEVOPS_NODE_TOKEN` - auth token for the NPM registry

`AUTO_DEVOPS_PYTHON_REGISTRY` - custom Pip registry (default: `pypi.org/simple`)

